package com.yunze.quartz.task.yunze.cardflow;

import com.yunze.common.mapper.yunze.YzAgentPackageMapper;
import com.yunze.common.mapper.yunze.YzCardPackageMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 定时任务 同步改字段数据【表1：yz_card_package 2：yz_agent_package】 卡总数
 */
@Slf4j
@Component("cardPostageCount")
public class CardPostageCount {

    @Resource
    private YzCardPackageMapper yzCardPackageMapper;
    @Resource
    private YzAgentPackageMapper yzAgentPackageMapper;


    public void synchronization() {
        Map<String, Object> Rmap = new HashMap<>();

        List<Map<String, Object>> Cardlist = yzCardPackageMapper.cardCount(Rmap);//平台资费组
        List<Map<String, Object>> AgentList = yzAgentPackageMapper.agentCount(Rmap);//代理资费组

        if (Cardlist != null && AgentList != null) {
            try {
                for (int i = 0; i < Cardlist.size(); i++) {
                    Map<String, Object> obj = Cardlist.get(i);
                    if (obj.get("package_id") != null) {
                        Integer integer = yzCardPackageMapper.upCountC(obj); //进行修改 平台资费组 卡总数
                        log.info(integer + "平台资费组--->数据操作成功");
                    }
                }
                for (int i = 0; i < AgentList.size(); i++) {
                    Map<String, Object> obj = AgentList.get(i);
                    if (obj.get("package_id") != null) {
                        Integer integer = yzAgentPackageMapper.upCountA(obj); //进行修改 代理资费组 卡总数
                        log.info(integer + "代理资费组--->数据操作成功");
                    }
                }
            } catch (Exception e) {
                System.out.println("操作异常" + e.getMessage());
            }
        }
    }
}


















